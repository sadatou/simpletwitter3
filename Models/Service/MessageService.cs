﻿using MySql.Data.MySqlClient;
using SimpleTwitter3.Models.Dao;
using SimpleTwitter3.Models.Dto;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;

namespace SimpleTwitter3.Models.Service
{
    public class MessageService
    {
        private static readonly string connectionStrings = ConfigurationManager.ConnectionStrings["mysql"].ConnectionString;

        public static void Insert(Message message)
        {
            MySqlConnection connection = null;
            MySqlTransaction transaction = null;
            try
            {
                connection = new MySql.Data.MySqlClient.MySqlConnection(connectionStrings);
                connection.Open();
                transaction = connection.BeginTransaction(IsolationLevel.ReadCommitted);

                MessageDao.InsertMessage(connection, message);
                transaction.Commit();
            }
            catch (SqlException ex)
            {
                if (transaction != null)
                {
                    transaction.Rollback();
                }
                System.Diagnostics.Debug.WriteLine("例外発生:" + ex.Message);
                throw ex;
            }
            finally
            {
                connection.Close();
            }
        }

        public static List<UserMessage> Select()
        {
            MySqlConnection connection = null;
            MySqlTransaction transaction = null;
            try
            {
                connection = new MySql.Data.MySqlClient.MySqlConnection(connectionStrings);
                connection.Open();
                transaction = connection.BeginTransaction(IsolationLevel.ReadCommitted);

                List<UserMessage> messages = UserMessageDao.Select(connection);
                transaction.Commit();

                return messages;
            }
            catch (SqlException ex)
            {
                if (transaction != null)
                {
                    transaction.Rollback();
                }
                System.Diagnostics.Debug.WriteLine("例外発生:" + ex.Message);
                throw ex;
            }
            finally
            {
                connection.Close();
            }
        }
    }
}

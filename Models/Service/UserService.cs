﻿using MySql.Data.MySqlClient;
using SimpleTwitter3.Models.Dao;
using SimpleTwitter3.Models.Dto;
using SimpleTwitter3.Models.Utils;
using System;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;

namespace SimpleTwitter3.Models.Service
{
    public class UserService
    {
        private static readonly string connectionStrings = ConfigurationManager.ConnectionStrings["mysql"].ConnectionString;

        public static void Insert(User user)
        {
            MySqlConnection connection = null;
            MySqlTransaction transaction = null;
            try
            {
                //パスワード暗号化
                user.Password = CipherUtil.encrypt(user.Password);

                connection = new MySql.Data.MySqlClient.MySqlConnection(connectionStrings);
                connection.Open();
                transaction = connection.BeginTransaction(IsolationLevel.ReadCommitted);

                UserDao.Insert(connection, user);
                transaction.Commit();
            }
            catch (SqlException ex)
            {
                if (transaction != null)
                {
                    transaction.Rollback();
                }
                System.Diagnostics.Debug.WriteLine("例外発生:" + ex.Message);
                throw ex;
            }
            finally
            {
                connection.Close();
            }
        }

        public static User Select(User user)
        {
            MySqlConnection connection = null;
            MySqlTransaction transaction = null;
            try
            {
                //パスワード暗号化
                user.Password = CipherUtil.encrypt(user.Password);

                connection = new MySql.Data.MySqlClient.MySqlConnection(connectionStrings);
                connection.Open();
                transaction = connection.BeginTransaction(IsolationLevel.ReadCommitted);

                User loginUser = UserDao.Select(connection, user);
                transaction.Commit();

                return loginUser;
            }
            catch (SqlException ex)
            {
                if (transaction != null)
                {
                    transaction.Rollback();
                }
                System.Diagnostics.Debug.WriteLine("例外発生:" + ex.Message);
                throw ex;
            }
            catch (InvalidOperationException ex)
            {
                if (transaction != null)
                {
                    transaction.Rollback();
                }
                System.Diagnostics.Debug.WriteLine("例外発生:" + ex.Message);
                throw ex;
            }
            finally
            {
                connection.Close();
            }
        }

        public static User Select(string accountOrEmail)
        {
            MySqlConnection connection = null;
            MySqlTransaction transaction = null;
            try
            {
                connection = new MySql.Data.MySqlClient.MySqlConnection(connectionStrings);
                connection.Open();
                transaction = connection.BeginTransaction(IsolationLevel.ReadCommitted);

                User user = UserDao.Select(connection, accountOrEmail);
                transaction.Commit();

                return user;
            }
            catch (SqlException ex)
            {
                if (transaction != null)
                {
                    transaction.Rollback();
                }
                System.Diagnostics.Debug.WriteLine("例外発生:" + ex.Message);
                throw ex;
            }
            catch (InvalidOperationException ex)
            {
                if (transaction != null)
                {
                    transaction.Rollback();
                }
                System.Diagnostics.Debug.WriteLine("例外発生:" + ex.Message);
                throw ex;
            }
            finally
            {
                connection.Close();
            }
        }

        public static void Update(User user)
        {
            MySqlConnection connection = null;
            MySqlTransaction transaction = null;
            try
            {
                //パスワードが入力されている場合
                if (!string.IsNullOrEmpty(user.Password))
                {
                    //パスワード暗号化
                    user.Password = CipherUtil.encrypt(user.Password);
                }

                connection = new MySql.Data.MySqlClient.MySqlConnection(connectionStrings);
                connection.Open();
                transaction = connection.BeginTransaction(IsolationLevel.ReadCommitted);

                UserDao.Update(connection, user);
                transaction.Commit();
            }
            catch (SqlException ex)
            {
                if (transaction != null)
                {
                    transaction.Rollback();
                }
                System.Diagnostics.Debug.WriteLine("例外発生:" + ex.Message);
                throw ex;
            }
            finally
            {
                connection.Close();
            }
        }
    }
}

﻿using MySql.Data.MySqlClient;
using SimpleTwitter3.Models.Dto;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Text;

namespace SimpleTwitter3.Models.Dao
{
    public class UserMessageDao
    {
        public static List<UserMessage> Select(MySqlConnection connection)
        {
            MySqlDataReader reader = null;

            StringBuilder sql = new StringBuilder();
            sql.Append("SELECT ");
            sql.Append("    messages.id as id, ");
            sql.Append("    messages.text as text, ");
            sql.Append("    users.id as user_id, ");
            sql.Append("    users.account as account, ");
            sql.Append("    users.name as name, ");
            sql.Append("    messages.created_date as created_date ");
            sql.Append("FROM messages ");
            sql.Append("INNER JOIN users ");
            sql.Append("ON messages.user_id = users.id ");
            sql.Append("ORDER BY created_date DESC");

            MySqlCommand command = new MySqlCommand(sql.ToString(), connection);

            try
            {
                reader = command.ExecuteReader();
                List<UserMessage> messages = ToUserMessages(reader);

                return messages;
            }
            catch (SqlException ex)
            {
                throw ex;
            }
            finally
            {
                reader.Close();
            }
        }

        private static List<UserMessage> ToUserMessages(MySqlDataReader reader) 
        {
            try
            {
                List<UserMessage> messages = new List<UserMessage>();
                while (reader.Read())
                {
                    UserMessage message = new UserMessage();
                    message.Id = (int)reader["id"];
                    message.Text = (string)reader["text"];
                    message.UserId = (int)reader["user_id"];
                    message.Account = (string)reader["account"];
                    message.Name = (string)reader["name"];
                    message.CreatedDate = (DateTime)reader["created_date"];

                    messages.Add(message);
                }
                return messages;
            }
            catch (SqlException ex)
            {
                throw ex;
            }
        }
    }
}
﻿using MySql.Data.MySqlClient;
using SimpleTwitter3.Models.Dto;
using SimpleTwitter3.Commons;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Text;

namespace SimpleTwitter3.Models.Dao
{
    public class UserDao
    {
        public static void Insert(MySqlConnection connection, User user)
        {
            StringBuilder sql = new StringBuilder();
            sql.Append("INSERT INTO users ( ");
            sql.Append("    account, ");
            sql.Append("    name, ");
            sql.Append("    email, ");
            sql.Append("    password, ");
            sql.Append("    description, ");
            sql.Append("    created_date, ");
            sql.Append("    updated_date ");
            sql.Append(") VALUES ( ");
            sql.Append("    @account, ");
            sql.Append("    @name, ");
            sql.Append("    @email, ");
            sql.Append("    @password, ");
            sql.Append("    @description, ");
            sql.Append("    CURRENT_TIMESTAMP, ");
            sql.Append("    CURRENT_TIMESTAMP ");
            sql.Append(")");

            MySqlCommand command = new MySqlCommand(sql.ToString(), connection);

            command.Parameters.Add(new MySqlParameter("account", user.Account));
            command.Parameters.Add(new MySqlParameter("name", user.Name));
            command.Parameters.Add(new MySqlParameter("email", user.Email));
            command.Parameters.Add(new MySqlParameter("password", user.Password));
            command.Parameters.Add(new MySqlParameter("description", user.Description));

            try
            {
                command.ExecuteNonQuery();
            }
            catch (SqlException ex)
            {
                throw ex;
            }
        }

        public static User Select(MySqlConnection connection, User user)
        {
            MySqlDataReader reader = null;

            StringBuilder sql = new StringBuilder();
            sql.Append("SELECT * FROM users ");
            sql.Append("WHERE ( account = @account OR email = @email ) ");
            sql.Append("AND password = @password");

            MySqlCommand command = new MySqlCommand(sql.ToString(), connection);
            
            command.Parameters.Add(new MySqlParameter("account", user.Account));
            command.Parameters.Add(new MySqlParameter("email", user.Account));
            command.Parameters.Add(new MySqlParameter("password", user.Password));

            try
            {
                reader = command.ExecuteReader();
                List<User> users = ToUsers(reader);

                if (users.Count == 0)
                {
                    return null;
                } 
                else if (2 <= users.Count)
                {
                    throw new InvalidOperationException(ErrorMessage.USER_DUPLICATE);
                } 
                else
                {
                    return users[0];
                }
            }
            catch (SqlException ex)
            {
                throw ex;
            }
            finally
            {
                reader.Close();
            }
        }

        public static User Select(MySqlConnection connection, string accountOrEmail)
        {
            MySqlDataReader reader = null;

            StringBuilder sql = new StringBuilder();
            sql.Append("SELECT * FROM users ");
            sql.Append("WHERE ( account = @account OR email = @email )");

            MySqlCommand command = new MySqlCommand(sql.ToString(), connection);

            command.Parameters.Add(new MySqlParameter("account", accountOrEmail));
            command.Parameters.Add(new MySqlParameter("email", accountOrEmail));

            try
            {
                reader = command.ExecuteReader();
                List<User> users = ToUsers(reader);

                if (users.Count == 0)
                {
                    return null;
                }
                else if (2 <= users.Count)
                {
                    throw new InvalidOperationException(ErrorMessage.ACCOUNT_DUPLICATE);
                }
                else
                {
                    return users[0];
                }
            }
            catch (SqlException ex)
            {
                throw ex;
            }
            finally
            {
                reader.Close();
            }
        }

        private static List<User> ToUsers(MySqlDataReader reader)
        {
            try
            {
                List<User> users = new List<User>();
                while (reader.Read())
                {
                    User user = new User();
                    user.Id = (int)reader["id"];
                    user.Account = (string)reader["account"];
                    user.Name = (string)reader["name"];
                    user.Email = (string)reader["email"];
                    user.Password = (string)reader["password"];
                    user.Description = (string)reader["description"];
                    user.CreatedDate = (DateTime)reader["created_date"];
                    user.UpdatedDate = (DateTime)reader["updated_date"];

                    users.Add(user);
                }
                return users;
            }
            catch (SqlException ex)
            {
                throw ex;
            }
        }

        public static void Update(MySqlConnection connection, User user)
        {
            StringBuilder sql = new StringBuilder();
            sql.Append("UPDATE users SET ");
            sql.Append("    account = @account, ");
            sql.Append("    name = @name, ");
            sql.Append("    email = @email, ");
            //パスワードが入力されている場合
            if (!string.IsNullOrEmpty(user.Password))
            {
                sql.Append("password = @password, ");
            }
            sql.Append("    description = @description, ");
            sql.Append("    updated_date = CURRENT_TIMESTAMP ");
            sql.Append("WHERE id = @id");

            MySqlCommand command = new MySqlCommand(sql.ToString(), connection);

            command.Parameters.Add(new MySqlParameter("account", user.Account));
            command.Parameters.Add(new MySqlParameter("name", user.Name));
            command.Parameters.Add(new MySqlParameter("email", user.Email));
            //パスワードが入力されている場合
            if (!string.IsNullOrEmpty(user.Password))
            {
                command.Parameters.Add(new MySqlParameter("password", user.Password));
            }
            command.Parameters.Add(new MySqlParameter("description", user.Description));
            command.Parameters.Add(new MySqlParameter("id", user.Id));

            try
            {
                command.ExecuteNonQuery();
            }
            catch (SqlException ex)
            {
                throw ex;
            }
        }
    }
}
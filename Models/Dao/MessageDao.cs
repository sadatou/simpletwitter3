﻿using MySql.Data.MySqlClient;
using SimpleTwitter3.Models.Dto;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Web;

namespace SimpleTwitter3.Models.Dao
{
    public class MessageDao
    {
        public static void InsertMessage(MySqlConnection connection, Message message)
        {
            StringBuilder sql = new StringBuilder();
            sql.Append("INSERT INTO messages ( ");
            sql.Append("    user_id, ");
            sql.Append("    text, ");
            sql.Append("    created_date, ");
            sql.Append("    updated_date ");
            sql.Append(") VALUES ( ");
            sql.Append("    @user_id, ");
            sql.Append("    @text, ");
            sql.Append("    CURRENT_TIMESTAMP, ");
            sql.Append("    CURRENT_TIMESTAMP ");
            sql.Append(")");

            MySqlCommand command = new MySqlCommand(sql.ToString(), connection);

            command.Parameters.Add(new MySqlParameter("user_id", message.UserId));
            command.Parameters.Add(new MySqlParameter("text", message.Text));

            try
            {
                command.ExecuteNonQuery();
            }
            catch (SqlException ex)
            {
                throw ex;
            }
        }
    }
}
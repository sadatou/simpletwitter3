﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace SimpleTwitter3.Models.Dto
{
    public class User
    {
        [DisplayName("ID")]
        public int Id { get; set; }

        [DisplayName("アカウント名")]
        public string Account { get; set; }

        [DisplayName("名前")]
        public string Name { get; set; }

        [DisplayName("メールアドレス")]
        public string Email { get; set; }

        [DisplayName("パスワード")]
        public string Password { get; set; }

        [DisplayName("説明")]
        public string Description { get; set; }

        [DisplayName("作成日時")]
        public DateTime CreatedDate { get; set; }

        [DisplayName("更新日時")]
        public DateTime UpdatedDate { get; set; }
    }
}
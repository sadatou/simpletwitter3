﻿using System;
using System.ComponentModel;

namespace SimpleTwitter3.Models.Dto
{
    public class Message
    {
        [DisplayName("ID")]
        public int Id { get; set; }

        [DisplayName("ユーザーID")]
        public int UserId { get; set; }

        [DisplayName("つぶやき")]
        public string Text { get; set; }

        [DisplayName("作成日時")]
        public DateTime CreatedDate { get; set; }

        [DisplayName("更新日時")]
        public DateTime UpdatedDate { get; set; }
    }
}
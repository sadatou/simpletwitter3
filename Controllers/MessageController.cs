﻿using SimpleTwitter3.Models.Dto;
using SimpleTwitter3.Commons;
using SimpleTwitter3.Models.Service;
using System.Collections.Generic;
using System.Web.Mvc;

namespace SimpleTwitter3.Controllers
{
    public class MessageController : Controller
    {
        [HttpPost]
        public ActionResult Insert(Message message)
        {
            List<string> errorMessages = new List<string>();

            //入力が正しい場合
            if (IsValid(message.Text, errorMessages))
            {
                User loginUser = (User)Session["loginUser"];
                message.UserId = loginUser.Id;

                MessageService.Insert(message);
            }
            else
            {
                TempData["ErrorMessages"] = errorMessages;
                TempData["Message"] = message;
            }

            return RedirectToAction("index", "Top");
        }

        private bool IsValid(string text, List<string> errorMessages)
        {
            //つぶやきが未入力の場合
            if (string.IsNullOrEmpty(text))
            {
                errorMessages.Add(ErrorMessage.MESSAGE_NOT_ENTERED);
            }
            //つぶやきが140文字以下ではない場合
            else if (140 < text.Length)
            {
                errorMessages.Add(ErrorMessage.MESSAGE_LIMIT);
            }

            if (errorMessages.Count != 0)
            {
                return false;
            }
            return true;
        }
    }
}

﻿using SimpleTwitter3.Models.Dto;
using SimpleTwitter3.Models.Service;
using SimpleTwitter3.ViewModels;
using System.Collections.Generic;
using System.Web.Mvc;

namespace SimpleTwitter3.Controllers
{
    public class TopController : Controller
    {
        public ActionResult Index()
        {
            TopViewModel view = new TopViewModel();
            view.ErrorMessage = (List<string>)TempData["ErrorMessages"] ?? new List<string>();
            view.Message = (Message)TempData["Message"] ?? new Message();
            view.UserMessage = MessageService.Select();
            view.User = (User)Session["LoginUser"];

            return View(view);
        }
    }
}
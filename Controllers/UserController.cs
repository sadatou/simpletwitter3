﻿using SimpleTwitter3.Models.Dto;
using SimpleTwitter3.Commons;
using SimpleTwitter3.Models.Service;
using SimpleTwitter3.ViewModels;
using System.Collections.Generic;
using System.Text.RegularExpressions;
using System.Web.Mvc;

namespace SimpleTwitter3.Controllers
{
    public class UserController : Controller
    {
        public ActionResult Signup()
        {
            UserViewModel view = new UserViewModel();
            view.ErrorMessage = (List<string>)TempData["ErrorMessages"] ?? new List<string>();
            view.User = (User)TempData["User"] ?? new User();

            return View(view);
        }

        public ActionResult Setting()
        {
            UserViewModel view = new UserViewModel();
            view.ErrorMessage = (List<string>)TempData["ErrorMessages"] ?? new List<string>();
            view.User = (User)TempData["User"] ?? (User)Session["LoginUser"];

            return View(view);
        }

        [HttpPost]
        public ActionResult Insert(User user)
        {
            List<string> errorMessages = new List<string>();
            user.Id = 0;

            //入力に誤りがある場合
            if (!IsValid(user, errorMessages))
            {
                TempData["ErrorMessages"] = errorMessages;
                TempData["User"] = user;
                return RedirectToAction("Signup", "User");
            }

            //ユーザー登録
            UserService.Insert(user);
            return RedirectToAction("Index", "Top");
        }

        [HttpPost]
        public ActionResult Update(User user)
        {
            List<string> errorMessages = new List<string>();
            User loginUser = (User)Session["LoginUser"];
            user.Id = loginUser.Id;

            //入力に誤りがある場合
            if (!IsValid(user, errorMessages))
            {
                TempData["ErrorMessages"] = errorMessages;
                TempData["User"] = user;
                return RedirectToAction("Setting", "User");
            }

            //ユーザー更新
            UserService.Update(user);
            Session["LoginUser"] = user;
            return RedirectToAction("Index", "Top");
        }

        private bool IsValid(User user, List<string> errorMessages)
        {
            //名前が未入力の場合
            if (string.IsNullOrEmpty(user.Name))
            {
                user.Name = string.Empty;
            }
            //名前が20文字以下ではない場合
            else if (20 < user.Name.Length)
            {
                errorMessages.Add(ErrorMessage.NAME_LIMIT);
            }
            //アカウント名が未入力の場合
            if (string.IsNullOrEmpty(user.Account))
            {
                errorMessages.Add(ErrorMessage.ACCOUNT_NOT_ENTERED);
            }
            //アカウント名が半角英数字6文字以上20文字以下ではない場合
            else if (!Regex.IsMatch(user.Account, "^[0-9a-zA-Z]{6,20}$"))
            {
                errorMessages.Add(ErrorMessage.ACCOUNT_NOT_MATCHES);
            }
            else
            {
                //アカウント名が重複している場合
                User duplicateUser = UserService.Select(user.Account);
                if (duplicateUser != null)
                {
                    if ((user.Id == 0) || (user.Id != duplicateUser.Id))
                    {
                        errorMessages.Add(ErrorMessage.ACCOUNT_DUPLICATE);
                    }
                }
            }
            //パスワードが未入力の場合
            if (string.IsNullOrEmpty(user.Password))
            {
                //ユーザー登録の場合
                if (user.Id == 0) {
                    errorMessages.Add(ErrorMessage.PASSWORD_NOT_ENTERED);
                }
            }
            //パスワードが半角6文字以上20文字以下ではない場合
            else if (!Regex.IsMatch(user.Password, "^[!-~]{6,20}$"))
            {
                errorMessages.Add(ErrorMessage.PASSWORD_NOT_MATCHES);
            }
            //メールアドレスが未入力の場合
            if (string.IsNullOrEmpty(user.Email))
            {
                user.Email = string.Empty;
            }
            //メールアドレスが半角50文字以下ではない場合
            else if (!Regex.IsMatch(user.Email, "^[!-~]{1,50}$"))
            {
                errorMessages.Add(ErrorMessage.EMAIL_NOT_MATCHES);
            }
            else
            {
                //メールアドレスが重複している場合
                User duplicateUser = UserService.Select(user.Email);
                if (duplicateUser != null)
                {
                    if ((user.Id == 0) || (user.Id != duplicateUser.Id))
                    {
                        errorMessages.Add(ErrorMessage.EMAIL_DUPLICATE);
                    }
                }
            }
            //説明が未入力の場合
            if (string.IsNullOrEmpty(user.Description))
            {
                user.Description = string.Empty;
            }

            if (errorMessages.Count != 0)
            {
                return false;
            }
            return true;
        }
    }
}

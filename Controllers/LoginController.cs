﻿using SimpleTwitter3.Models.Dto;
using SimpleTwitter3.Models.Service;
using SimpleTwitter3.ViewModels;
using System.Collections.Generic;
using System.Web.Mvc;
using SimpleTwitter3.Commons;

namespace SimpleTwitter.Controllers
{
    public class LoginController : Controller
    {
        public ActionResult Index()
        {
            UserViewModel view = new UserViewModel();
            view.ErrorMessage = (List<string>)TempData["ErrorMessages"] ?? new List<string>();
            view.User = (User)TempData["User"] ?? new User();

            return View(view);
        }

        public ActionResult Logout()
        {
            Session.Abandon();
            return RedirectToAction("Index", "Top");
        }

        [HttpPost]
        public ActionResult Selcet(User user)
        {
            List<string> errorMessages = new List<string>();
            User loginUser = null;

            //ログイン情報が入力されている場合
            if (IsValid(user, errorMessages))
            {
                //ログインユーザー取得
                loginUser = UserService.Select(user);

                //ログインユーザーが存在しない場合
                if (loginUser == null)
                {
                    errorMessages.Add(ErrorMessage.LOGIN_FAILED);
                }
            }

            //入力に誤りがある場合
            if (errorMessages.Count != 0)
            {
                TempData["ErrorMessages"] = errorMessages;
                TempData["User"] = user;
                return RedirectToAction("Index", "Login");
            }

            Session["LoginUser"] = loginUser;
            return RedirectToAction("Index", "Top");
        }

        private bool IsValid(User user, List<string> errorMessages)
        {
            //アカウント名が未入力の場合
            if (string.IsNullOrEmpty(user.Account))
            {
                errorMessages.Add(ErrorMessage.ACCOUNT_NOT_ENTERED);
            }
            //パスワードが未入力の場合
            if (string.IsNullOrEmpty(user.Password))
            {
                errorMessages.Add(ErrorMessage.PASSWORD_NOT_ENTERED);
            }

            if (errorMessages.Count != 0)
            {
                return false;
            }
            return true;
        }
    }
}

﻿using SimpleTwitter3.Models.Dto;
using System.Collections.Generic;

namespace SimpleTwitter3.ViewModels
{
    public class TopViewModel
    {
        public User User { get; set; }
        public Message Message { get; set; }
        public IEnumerable<UserMessage> UserMessage { get; set; }
        public IEnumerable<string> ErrorMessage { get; set; }
    }
}
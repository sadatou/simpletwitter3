﻿using SimpleTwitter3.Models.Dto;
using System.Collections.Generic;

namespace SimpleTwitter3.ViewModels
{
    public class UserViewModel
    {
        public User User { get; set; }
        public IEnumerable<string> ErrorMessage { get; set; }
    }
}